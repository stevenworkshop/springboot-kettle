package com.kettle;

import groovy.util.logging.Slf4j;
import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.job.Job;
import org.pentaho.di.job.JobMeta;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.springframework.stereotype.Service;

/**
 * @ClassName KettleService
 * @Author YanZhen
 * @Date 2021/6/17
 * @Version V1.0
 **/
@Slf4j
@Service
public class KettleService {

    public static void main(String[] args) throws KettleException {
        //runJob();
        new KettleService().runTransfer();
        System.out.println("again");
        new KettleService().runTransferRelative();
    }


    public  boolean runTransfer() {
        boolean kettlesuccess=true;
        try {
            KettleEnvironment.init();
            TransMeta tm = new TransMeta("E:\\2021年部门内部管理\\定时从公司数据库同步作业到51库.ktr");
            Trans trans = new Trans(tm);
            trans.execute(null);
            trans.waitUntilFinished();
        } catch (KettleException ex) {
            System.out.println(ex);
            kettlesuccess=false;
        }
        return kettlesuccess;
    }
    public  void runTransferRelative() {
        try {
            KettleEnvironment.init();

            String property = System.getProperty("user.dir");
            String path = property + "/src/main/resources/static/files";

            TransMeta tm = new TransMeta("E:\\2021年部门内部管理\\定时从公司数据库同步作业到51库.ktr");
            Trans trans = new Trans(tm);
            trans.execute(null);
            trans.waitUntilFinished();
        } catch (KettleException ex) {
            System.out.println(ex);
        }
    }

    public static void runJob() throws KettleException {
        KettleEnvironment.init();
        // 任务元对象  fileName为作业的绝对路径C:\Users\Administrator\Desktop\参数传递测试作业.kjb

        JobMeta jm = new JobMeta("/Users/scott/Documents/db-user-transfer-zuo.kjb", null);
        // 任务
        Job job = new Job(null, jm);
        // 传参
      /*  job.setVariable("beginTime", beginTime);
        job.setVariable("endTime", endTime);*/
        // 开始任务
        job.start();
        // 等待任务结束
        job.waitUntilFinished();
    }
}
