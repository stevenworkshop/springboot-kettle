package com.kettle;

import groovy.util.logging.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/sys")
@Slf4j

//@Api(tags = "定时任务接口")
public class KettleController {
    @Autowired
    private KettleService kettleService;
    private String AppKey="hello@kitty123";

    // http://localhost:8167/sys/qdkettle
    @GetMapping("/qdkettle")
    public String qdkettleService(String appKey) {
        String result="";
        boolean kettlesuccess=true;
        if(appKey.equalsIgnoreCase(AppKey)) {
            kettlesuccess= kettleService.runTransfer();
            if(kettlesuccess) {
                result = "调用成功，kettle执行正常";
            }else {
                result="调用成功，kettle执行异常";
            }
        }else {
            System.out.println("用户名不对");
            result="调用成功，但appKey不对";
        }
        return result;
    }

    // http://localhost:8080/sys/list
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public void queryPageList() {
        kettleService.runTransfer();
    }

    // http://localhost:8080/sys/baidu
    //客户端进行跳转
    @RequestMapping(value = "/baidu", method = RequestMethod.GET)
    public void testRed(HttpServletResponse response) throws Exception{
        response.sendRedirect("https://www.baidu.com");
    }
    // 可以成功跳转
    //客户端进行跳转
    @RequestMapping(value = "/baiduX", method = RequestMethod.GET)
    public void testRedX(HttpServletResponse response) throws Exception{
        //response.sendRedirect("https://www.baidu.com");
        response.sendRedirect("https://sebxs3jehc.jiandaoyun.com/f/61404d085a45700008270308");

    }

}
