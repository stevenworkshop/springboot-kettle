package com.kettle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName com.kettle.KettleTestApplication
 * @Author YanZhen
 * @Date 2021/6/17
 * @Version V1.0
 **/
@SpringBootApplication
public class KettleTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(KettleTestApplication.class, args);
    }



}
