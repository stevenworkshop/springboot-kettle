# springboot-kettle

#### 介绍
springboot集成kettle7.1
通过内嵌lib 的方式。
修改为通过服务的方式来提供Kettle的运行。避免jar之间的冲突
当前的版本存在的问题
    删除了log记录的依赖，导致出现问题。
	解决方法，就是回退到上一个版本。
	但是，需要把本版本中修改过的KettleController的方法复制到以前的版本中。

因为执行的kettle任务需要依赖com的机器。暂时在家就不修改了。后面到单位进行修改。

#### 软件架构
软件架构说明
kettle处理的时候，不需要pom文件的支持，只需要lib库的支持。

从网上下载JeecgBoot。
客户端生成dist
db执行一次
服务端修改端口号，打成jar
服务器增加一个定时任务的类。
修改Nginx的配置文件

在把kettleservice的任务打成一个jar。发布到服务器上运行。

以后增加定时任务的时候。
主客户端需要增加一个调用类。
然后在kettleservice中增加一个调kettle的服务。



#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
